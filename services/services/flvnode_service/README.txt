README Flvnode_service Drupal Module

This module is for Drupal 5.x only!

Installation
============

Activate the module under /admin/build/modules

You will see available service under /admin/build/services

Important: configure the services to not use keys! Visit /admin/build/services/settings to deselect the "Use keys" checkbox. Currently we just use session id for authentication between Flash recorder and Drupal.


More information
================

Installation instructions: http://developer.test1212.org/doc/installing-videonoderecorder-drupal-module

Support forum: http://developer.test1212.org/forum

General user's guide: http://developer.test1212.org/doc/users-guide 

Live test: http://test1212.org 
