// <![CDATA[
   
      // get our flash movie object
      var flashMovie;
      function init() {
         if (document.getElementById) {
            flashMovie = document.getElementById("swfwebcamVideoRec");
	    //alert(flashMovie);
         }
      }
      
      // wait for the page to fully load before initializing
      window.onload = init;
   
      // for updating the title
      function updateTitle() {
         if (flashMovie) {
            var txt = document.getElementById("edit-title").value;
	    //alert(txt);
            flashMovie.updateTitle(txt);
         }
      }
      
   // ]]>
