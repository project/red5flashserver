README Red5 Flash Server Drupal Module
======================================

This module is a module package for Drupal 5.x, containing several modules like videonoderecorder, videocommentrecorder, audionoderecorder, audiocommentrecorder. Some of them are not released yet, but we are working on it.


Installation instructions
=========================

Red5 Flash Server is a module package for Drupal 5.x, containing several modules like videonoderecorder, videocommentrecorder, audionoderecorder, audiocommentrecorder, flvnode_service. Some of them are not released yet, but we are working on it. Each module comes with a pre-compiled Flash recorder (SWF) residing in ../recorder sub-directory.

First of all, you will need a Drupal 5.x installation.

Download the Red5 Flash Server module and extract it into your Drupal 5.x modules directory.

The Red5 Flash Server Drupal Module is dependent on other Drupal modules (download corresponding 5.x module versions). Here is the list:

http://drupal.org/project/services
http://drupal.org/project/amfphp
http://drupal.org/project/swftools

Download and extract these modules in your Drupal module directory. Go to the admin panel /admin/build/modules and enable them. Follow the installation instructions for each module. Don't forget to activate the SWF filter for the input format "Filtered HTML" under /admin/settings/filters. The filter is used to embed the configured Flash video player to play your recorded videos.

Configure the services module: go to /admin/build/services/settings and uncheck the "Use keys" checkbox. Currently we only use session id as authentication method between the Flash recorder and Drupal.

Go to /admin/build/modules and enable the Upload module (part of core Drupal modules). This module is used to attach the recorded Flash video and thumbnail picture to your node.

Go to /admin/build/modules and enable the Video node recorder module (and any other modules from the Red5 Flash Server package you like) as well as the FLV node service module from the Services - services package.

Set the user permissions under /admin/user/access (module names are videonoderecorder, videocommentrecorder, audionoderecorder and audiocommentrecorder).

Change the settings of the module under /admin/settings/webcamvideo. You will need the IP address of your Red5 Flash Server (or any third party Red5 Flash Server you have access to). Additionally you can choose the content type which should be used to embed and attach Flash videos.

Once Flash video files are recorded, they will be downloaded and played progressively. The Red5 video streaming capability is not used (at least for now). A symbolic link (in Linux) should be created in the files directory of Drupal root. A directory listing in the files directory would look like that:

flv -> /usr/local/red5/webapps/test1212/streams/<yourdomain.com>

Flash videos will be recorded by Red5 in exactly that directory. By creating the symbolic link the Flash video files are available in the Drupal files directory at the same time.

Flash video files and thumbnail pictures will be loaded by the Flash video player using the directory path: http://yourdomain.com/files/flv/<drupal user id>/<flash video filename>.flv

That's all on the Drupal module side. If everything is configured well, you can now record your own videos under /node/add/webcamvideo.


More information
================

Installation instructions: http://developer.test1212.org/doc/installing-red5-flash-server-drupal-module 

Support forum: http://developer.test1212.org/forum

General user's guide: http://developer.test1212.org/doc/users-guide 

Live test: http://test1212.org 
